## Client for MeowMeowDesu Minecraft

#### Как запустить
1. Скачайте [клиент](https://gitlab.com/MeowMeowDesu/MinecraftClient/tags), распакуйте
2. В папке файл есть файл **FreeLauncher.properties**, в нем есть строчка `installation_dir=ПУТЬ`. Измените `ПУТЬ` на путь до вашей папки, заменив один слеш (/) на два (//).  
Например: `D:\Program Files (x86)\.minecraft` → `D\:\\Program Files (x86)\\.minecraft`
То есть должно получиться `installation_dir=D\:\\Program Files (x86)\\.minecraft`
3. Запустите **launcher.exe**, выберите в профилях *MeowMeowDesu*.
4. В правом нижнем углу есть кнопка Switch user, нажмите ее, затем Log Out, затем введите свой ник в поле с логином и залогиньтесь.
5. Нажмите кнопку **play**